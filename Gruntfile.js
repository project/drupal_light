/**
 * @file
 */
module.exports = function(grunt) {
  require('time-grunt')(grunt);
  require('jit-grunt')(grunt, {
    sasslint: 'grunt-sass-lint'
  });

  // This is where we configure each task that we'd like to run.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      // This is where we set up all the tasks we'd like grunt to watch for changes.
      scripts: {
        files: ['js/source/{,*/}*.js'],
        tasks: ['uglify'],
        options: {
          spawn: false
        }
      },
      images: {
        files: ['images/source/{,*/}*.{png,jpg,gif}'],
        tasks: ['imagemin'],
        options: {
          spawn: false
        }
      },
      vector: {
        files: ['images/source/{,*/}*.svg'],
        tasks: ['svgmin'],
        options: {
          spawn: false
        }
      },
      css: {
        files: ['sass/{,*/}*.{scss,sass}'],
        tasks: ['sass:dev', 'autoprefixer']
      }
    },
    uglify: {
      // Minify all js files
      options: {
        sourceMap: true,
        mangle: false
      },
      my_target: {
        files: [{
          expand: true,
          cwd: 'js/source',
          src: '{,*/}*.js',
          dest: 'js/build'
        }]
      }
    },
    imagemin: {
      // Optimize all images
      dynamic: {
        files: [{
          expand: true,
          cwd: 'images/source/',
          src: ['{,*/}*.{png,jpg,gif}' ],
          dest: 'images/optimized/'
        }]
      }
    },
    svgmin: {
      options: {
        plugins: [{
          removeViewBox: false
        }, {
          removeUselessStrokeAndFill: false
        }]
      },
      dist: {
        files: [{
          expand: true,
          cwd: 'images/source/',
          src: ['{,*/}*.svg' ],
          dest: 'images/optimized/'
        }]
      }
    },
    sass: {
      dev: {
        options: {
          sourceMap: true,
          // This controls the compiled css and can be changed to nested, compact or compressed.
          outputStyle: 'expanded',
          precision: 5
        },
        files: {
          'css/essential.css': 'sass/essential.scss',
          'css/non-essential.css': 'sass/non-essential.scss',
          'css/header.css': 'sass/header.scss',
          'css/footer.css': 'sass/footer.scss',
          'css/print.css': 'sass/print.scss'
        }
      },
      prod: {
        options: {
          sourceMap: false,
          // This controls the compiled css and can be changed to nested, compact or compressed.
          outputStyle: 'compressed',
          precision: 5
        },
        files: '<%= sass.dev.files %>'
      }
    },
    autoprefixer: {
      options: {
        browsers: ['last 2 versions', 'ie 10', 'ie 9']
      },
      dist: {
        files: {
          'css/style.css': 'css/style.css',
//          'css/components/tabs.css': 'css/components/tabs.css',
//          'css/components/messages.css': 'css/components/messages.css',
          'css/print.css': 'css/print.css'
        }
      }
    },
    browserSync: {
      dev: {
        bsFiles: {
          src : [
            'css/{,*/}*.css',
            'templates/{,*/}*.twig',
            'images/optimized/{,*/}*.{png,jpg,gif,svg}',
            'js/build/{,*/}*.js',
            '*.theme'
          ]
        },
        options: {
          watchTask: true,
          /* Change this to "true" if you'd like the css to be injected rather than a browser refresh.
           * In order for this to work with Drupal you will need to install https://drupal.org/project/link_css.
           * Keep in mind though that this should not be run on a production site.
           */
          injectChanges: false
        }
      }
    },
    sasslint: {
      theme: [
        'sass/**/*.scss'
      ],
      options: {
        configFile: 'sass-lint.yml'
      }
    },
    parker: {
      options: {
        metrics: [
          "TotalStylesheetSize",
          "TotalSelectors",
          "SpecificityPerSelector",
          "SelectorsPerRule",
          "IdentifiersPerSelector"
        ]
      },
      src: [
        'css/{,*/}*.css'
      ]
    }
  });

  // Now that we've loaded the package.json and the node_modules we set the base path
  // for the actual execution of the tasks
  // grunt.file.setBase('/')

  // This is where we tell Grunt what to do when we type "grunt" into the terminal.
  // Note: if you'd like to run and of the tasks individually you can do so by typing 'grunt mytaskname' alternatively
  // you can type 'grunt watch' to automatically track your files for changes.
  grunt.registerTask('check-css', ['sass:prod', 'parker']);
  grunt.registerTask('theme', ['imagemin','svgmin', 'sass:prod', 'autoprefixer', 'uglify']);
  grunt.registerTask('dev', ['sass:dev', 'autoprefixer', 'watch']);
  grunt.registerTask('default', ['browserSync','watch']);
};
