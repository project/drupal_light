window.onload = function() {
  let links = document.getElementsByTagName("link");
  let linkCount = links.length;

  for (var i = 0; i < linkCount; i++) {
    if (links[i].getAttribute("media") === "none") {
      links[i].setAttribute("media", "screen");
    }
  }
};
